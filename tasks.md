# Model Evaluation - Sara
- Metrics (Accuracy, Precision?)
- First model - who will even buy?
- Moving threshold?

# Feature Selection - Florentin

# Pipeline and Model - Tara
- Division between categorical and numerical features
- Logistic Regression
- Decision Tree / Random Forest

# Feature Building - Florentin
- Do we need more features

# Create a sample data set - Tara

# Build a business case - Sara
- What is the intention of the model and what do we want to give to the client